# Shopping Cart - Cabify Challenge


Created by Numa Nicolás Pigretti

> Please if you have any suggestions or comments let me known.

> This challenge was made with a lot of effort and love, so i hope that you enjoy this challenge as me doing it.

> Sorry for the -eye killing- low resolution images for the modal, I couldn't find better ones in the project besides the shirt.

## Tech

This Challenge uses a number of open source projects to work properly:

- [React] - To make user interfaces
- [React-dom] To make global modals
- [Redux] - To make the app easy to grow
- [React-Redux] - To connect React with Redux
- [Styled-Components] - To make an enhance to sass 
- [Typescript] - To make react strongly typed, giving better tooling at any scale

## Installation and initiation

```sh
npm install 
npm start
(default port: 3000)
```
