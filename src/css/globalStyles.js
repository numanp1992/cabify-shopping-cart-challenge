import { createGlobalStyle } from 'styled-components'

export const theme = {
	colours: {
		H2productList: '#a6a7b3',
		H2global: '#717285',
		body: '#212240',
		productCode: '#a6a7b3',
		productPrice: '#000000',
		H1product: '#7350ff',
		summary: '#212240',
		summaryTotal: '#212240',
		white: '#ffffff',
		borderShirt: '#cacad1',
		borderQuantity: '#dbdbe0',
	}
};

export const sizes = {
	sm: 768,
	md: 1180,
};

export const media = {
	mobile: `(min-width: 0px) and (max-width: ${sizes.sm - 1}px)`,
	desktop: `(min-width: ${sizes.md}px)`,
};

export const getBrowser = () => { 
	var sBrowser, sUsrAg = navigator.userAgent;
	if(sUsrAg.indexOf('Chrome') > -1) {
		sBrowser = 'Chrome';
	} else if (sUsrAg.indexOf('Safari') > -1) {
		sBrowser = 'Safari';
	} else if (sUsrAg.indexOf('Opera') > -1) {
		sBrowser = 'Opera';
	} else if (sUsrAg.indexOf('Firefox') > -1) {
		sBrowser = 'Firefox';
	} else if (sUsrAg.indexOf('MSIE') > -1) {
		sBrowser = 'Explorer';
	}
	return sBrowser;
};


export const GlobalStyles = createGlobalStyle`
	html, body, div, span, applet, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, big, cite, code,
	del, dfn, em, img, ins, kbd, q, s, samp,
	small, strike, strong, sub, sup, tt, var,
	b, u, i, center,
	dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	table, caption, tbody, tfoot, thead, tr, th, td,
	article, aside, canvas, details, embed, 
	figure, figcaption, footer, header, hgroup, 
	menu, nav, output, ruby, section, summary,
	time, mark, audio, video {
		padding: 0;
		border: 0;
		font-size: 100%;
		vertical-align: baseline;
	}
	body {
		@font-face {
			font-family: 'Hind', sans-serif;
			src: url("./fonts/Hind-Light.ttf") format('truetype');
			font-weight: 'light';
			font-style: 'normal';
		};
		font-family: 'Hind', sans-serif;
		margin: 0;
	}
	article, aside, details, figcaption, figure, 
	footer, header, hgroup, menu, nav, section {
		display: block;
	}
	body {
		line-height: 1;
	}
	ol, ul {
		list-style: none;
	}
	blockquote, q {
		quotes: none;
	}
	blockquote:before, blockquote:after,
	q:before, q:after {
		content: '';
		content: none;
	}
	table {
		border-collapse: collapse;
		border-spacing: 0;
	}
	button {
		outline: none;
		border: none;
		background: none;
		padding: 0;
		cursor: pointer;
		&:focus:active{
			outline: none;
		}
	}
`;