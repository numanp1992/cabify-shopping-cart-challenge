import React from 'react';
import ReactDOM from 'react-dom';

//global styles css
import {theme, GlobalStyles, getBrowser, sizes} from './css/globalStyles';

//grapper styled components
import { ThemeProvider } from 'styled-components';

//grappper redux
import { Provider } from 'react-redux'
import { createStore } from 'redux';
import { rootReducer } from './store/reducers';

//css index
import './index.css';

//global app
import App from './App';

import reportWebVitals from './reportWebVitals';

import { DISCOUNTS } from './database/database'

const store = createStore(
	rootReducer,
	// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );

	let device = window.screen.width < sizes.md ? 'mobile' : 'desktop'
	console.log('challenge by Numa Pigretti for Cabify, you are in the browser ' + getBrowser() + ' and on ' + device)
	console.log(`PSSS, use ${ Object.keys(DISCOUNTS) } to get a discount! ;)`)
	
ReactDOM.render(
  <React.StrictMode>
	<ThemeProvider theme={theme}>
		<Provider store={store}>
			<App />
		</Provider>
		<GlobalStyles />
	</ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
