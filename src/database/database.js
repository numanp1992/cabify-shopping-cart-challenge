export const products = [
	{
		name: 'Shirt',
		productCode: 'X7R2OPX',
		productPrice: 20,
		totalPrice: 0,
		quantity: 0,
		currency: '€',
		hasDiscount: true,
		discountCondition: 'THREE_OR_MORE',
		discountLabel: 'x3 Shirt offer',
		discountShow: false,
		discountTotal: 0,
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vitae purus faucibus ornare suspendisse sed nisi lacus sed viverra. Ipsum nunc aliquet bibendum enim facilisis Dui faucibus in ornare quam viverra orci sagittis eu volutpat. Adipiscing elit ut aliquam purus sit amet luctus.'
	},
	{
		name: 'Mug',
		productCode: 'X2G2OPZ',
		productPrice: 5,
		totalPrice: 0,
		quantity: 0,
		currency: '€',
		hasDiscount: true,
		discountLabel: '2x1 Mug offer',
		discountCondition: 'TWO_FOR_ONE',
		discountShow: false,
		discountTotal: 0,
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vitae purus faucibus ornare suspendisse sed nisi lacus sed viverra. Ipsum nunc aliquet bibendum enim facilisis Dui faucibus in ornare quam viverra orci sagittis eu volutpat. Adipiscing elit ut aliquam purus sit amet luctus.'
	},
	{
		name: 'Cap',
		productCode: 'X3W2OPY',
		productPrice: 10,
		totalPrice: 0,
		quantity: 0,
		currency: '€',
		hasDiscount: false,
		discountTotal: 0,
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vitae purus faucibus ornare suspendisse sed nisi lacus sed viverra. Ipsum nunc aliquet bibendum enim facilisis Dui faucibus in ornare quam viverra orci sagittis eu volutpat. Adipiscing elit ut aliquam purus sit amet luctus.'
	},
]

export const DISCOUNTS= {
	'CABIFY10': {
		discountApplied: false,
		discountLabel: '10% discount',
	},
}