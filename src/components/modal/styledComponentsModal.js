import styled, { keyframes } from 'styled-components';

const handleOpenFrom = openFrom => {
	const open = {
		'top': 'align-items: flex-start;',
		'bottom': 'align-items: flex-end',
		'default': 'align-items: center;'
	};
	return open[openFrom] || open['default'];
};

const fadeIn = keyframes`
	from {
		transform: scale(.1);
		opacity: 0;
	}

	to {
		transform: scale(1);
		opacity: 1;
	}
`;

export const ModalBackground = styled.div`
	position: fixed;
	background-color: ${props => props.noBackgroundOpacity || 'rgba(0, 0, 0, 0.6)'};
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	z-index: 999;
	box-shadow: 0 2px 4px 0 rgba(50, 50, 93, 0.1);
	display: flex;
	justify-content: center;

	${({ openFrom }) => handleOpenFrom(openFrom)};
`;

export const ModalContainer = styled.div`
	background: #fff;
	border-radius: ${props => props.openFrom === 'top' ? '0px 0px 8px 8px' : props.openFrom === 'bottom' ? '8px 8px 0px 0px' : '8px'};
	box-sizing: border-box;
	position: absolute;
	display: flex;
	justify-content: center;
	align-items: center;
	overflow: hidden;
	width: ${props => props.width || '90%'};
	height: ${props => props.height || '90%'};
	padding: ${props => props.padding || '0px'};
	animation: ${fadeIn} .25s linear;
	transition: visibility .1s linear;
	transition-duration: 500ms;

`;

export const ModalButtonClose = styled.button`
	z-index:99;
	position: absolute;
	right: 16px;
	top: 16px;
	background-color: transparent;
	font-size: 14px;

	& > svg, img {
		width: 20px;
		height: 20px;
	}
`;
