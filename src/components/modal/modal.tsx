import React, {useContext} from "react";
import { createPortal } from "react-dom";

import { ModalContext } from "./modalContext";

import { 
	ModalBackground, 
	ModalContainer, 
	ModalButtonClose } 
from './styledComponentsModal';

import close from '../../img/close.svg';

const Modal = () => {

  let { modalContent, handleModal, modal } = useContext(ModalContext);

	document.addEventListener('keydown', (event) => {
		if (event.key === 'Escape' && modal) {
			handleModal();
		}
	})

	if (modal) {
		return createPortal(
			<ModalBackground>
			<ModalContainer>
				<ModalButtonClose
					onClick={() => handleModal()}
				>
				<img 
					src={ close }
					alt={ close }
				/>
				</ModalButtonClose>
				{modalContent}
			</ModalContainer>
			</ModalBackground>,
			document.querySelector("#modal-root")!
		);
	} else return null;
};

export default Modal;
