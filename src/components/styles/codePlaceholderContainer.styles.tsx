import styled from 'styled-components';

export const PlaceholderForm = styled.form`
    --searchButtonWidth: 75px;
    
    max-width: 320px;
    margin: 0 auto;
    overflow: hidden;
    position: absolute;
    right: 408px;
    top: 45px;
`;

export const InputPlaceholder = styled.input`
    border: 0;
    margin: 0;
    padding: 0.5rem calc(var(--searchButtonWidth) + 0.5rem) 0.5rem 0.5rem;
    border-radius: 8px;
    width: 100%;
    background: #ddd;
    -webkit-appearance: none;
    font-size: 13px;
    width: fit-content;
    :not(:placeholder-shown) ~ .button-discount {
        transform: translateX(calc(-1 * var(--searchButtonWidth)));
      }
`

export const InputButton = styled.button`
    border: 0;
    padding: 0.5rem;
    border-radius: 8px;
    position: absolute; 
    top: 0;
    left: 100%;
    width: var(--searchButtonWidth);
    transition: 0.2s;
    background: ${({ theme }) => theme.colours.H1product};;
    color: white;
    font-size: 13px;
    height: 100%;
`

export const InputLabel = styled.label`
    position: absolute;
    top: -9999px;
    left: -9999px;
`