import { css } from "styled-components";

export const Row = css`
	position: relative;
	display: flex;
	flex-flow: row nowrap;
	justify-content: space-between;
`;

export const TableHead = css`
	padding: 32px 0;
`;

export const Wrapper = css`
	padding: 32px 0;
`;

export const WrapperHalf = css`
	padding: 16px 0;
`;

export const Border = css`
	border-bottom: 1px solid rgba(33, 34, 64, 0.16);
`;