import styled from 'styled-components';

export const SummaryTotalCost = styled.span`
	flex-basis: 100%;
	text-transform: uppercase;
	font-size: 14px;
	line-height: 17px; 
	display: flex;
	align-items: center; 
`;

export const SummaryTotalPrice = styled.span`
	font-size: 20px;
	line-height: 25px;
	font-weight: bold;
`;

export const SummaryTotalButton = styled.button<{comeFromModal?: boolean}>`
  margin-top: 24px;
  padding-top: 16px;
  padding-bottom: 16px;
  width: ${({ comeFromModal }) => comeFromModal ? '100%' : '100%'};
  border-radius: 4px;
  background: #7350ff;
  color: #ffffff;
  font-size: 16px;
  font-weight: bold;
  line-height: 14px;
  cursor: pointer;
  :disabled {
    background: grey;
    cursor: inherit; 
  }
`;