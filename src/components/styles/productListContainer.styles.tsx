import styled from 'styled-components';

export const Product = styled.li<{Row: any}>`
	font-size: 16px;
	line-height: 24px;
	margin-bottom: 20px;
	
	${({ Row }) => Row && Row};
`;

export const ColProduct = styled.div<{width: string}>`
	width: ${({ width }) => width && width};
	display: flex;
	align-items: center;
`;

export const ProductImageContainer = styled.figure`
	display: flex;
	align-items: center;
	flex-flow: row nowrap;
	margin: 0px;
	cursor: pointer;
`;

export const ProductImage = styled.img`
  margin-right: 16px;
  width: 72px;
  height: 72px;
  border: 1px solid ${({ theme }) => theme.colours.borderShirt};
  border-radius: 4px;
`;

export const ProductName = styled.h1`
	font-size: 16px;
	line-height: 16px;
	color: ${({ theme }) => theme.colours.H1product};
`;

export const ModalProductDescription = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	align-items: center;
	flex-direction: row;
	align-content: space-around;
`;

export const ModalImageContainer = styled.div`
	height: 100%;
	width: 65%;
	display: flex;
	flex-direction: column;
	align-content: space-around;
	align-items: center;
`;

export const ModalInfoContainer = styled.div`
	height: 100%;
	width: 35%;
	display: flex;
	flex-direction: column;
	align-content: center;
	justify-content: center;
	align-items: center;
	padding: 5%;
	box-sizing: border-box;
`;

export const ModalPriceContainer = styled.div`
	display: flex;
	flex-direction: row;
	width: 100%;
	justify-content: space-between;
	font-style: normal;
	font-weight: 800;
	font-size: 18px;
`;

export const ModalDescriptionContainer = styled.div`
	width: 100%;
	border-top: 1px solid ${({ theme }) => theme.colours.borderQuantity};
	border-bottom: 1px solid ${({ theme }) => theme.colours.borderQuantity};
	padding: 10% 0px;
	margin: 10% 0px;
	line-height: 18px;
	letter-spacing: -0.01em;
`;

export const ModalProductCodeContainer = styled.div`
	width: 100%;
	font-size: 13px;
	line-height: 18px;
	letter-spacing: -0.01em;
	color: ${({ theme }) => theme.colours.productCode};;
`;

export const ModalCheckoutContainer = styled.h1`
	width: 90%;
	height:90%;
	display: flex;
	align-items: center;
	flex-direction: row;
	align-content: space-around;
	justify-content: center;
	flex-direction: column;
`;

export const SuccessImage = styled.img`
	width: 72px;
	height: 72px;
	margin: 30px;
	margin-top: 55px;
`;

export const ButtonCloseCheckout = styled.button`
	margin-top: 24px;
	padding-top: 16px;
	padding-bottom: 16px;
	width: 40%;
	height: 50px;
	border-radius: 4px;
	background: #7350ff;
	color: #ffffff;
	font-size: 16px;
	font-weight: bold;
	line-height: 14px;
	cursor: pointer;
`;

export const ModalProductImage = styled.img`
	width: 100%;
	height: 100%;
`;

export const ProductCode = styled.p`
	border-radius: 4px;
	color: #a6a7b3;
	letter-spacing: 0.13px;
	font-weight: 400;
	font-size: 12px;
	line-height: 0px;  
`;

export const ButtonContainer = styled.div`
	position: relative;
	left: -17px;
`;

export const CountButton = styled.button`
	padding: 0 8px;
	height: 40px;
	border: none;
	background: transparent;
	color: ${({ theme }) => theme.colours.H1product};
	font-weight: bold;
	cursor: pointer;
`;

export const ProductQuantityButton = styled.input`
	width: 40px;
	height: 40px;
	border: 2px solid ${({ theme }) => theme.colours.borderQuantity};
	border-radius: 4px;
	text-align: center;
`;

export const ProductPriceContainer = styled.span`
	color: ${({ theme }) => theme.colours.productPrice};
	font-size: 16px;
`;

export const ProductCurrency = styled.span`
	margin-left: 4px;
`;

