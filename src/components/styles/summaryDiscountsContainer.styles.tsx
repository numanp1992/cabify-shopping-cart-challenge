import styled from 'styled-components';

export const H2SummaryDiscounts = styled.h2`
	font-size: 16px;
	font-weight: bold;
	color: #717285;
	letter-spacing: 0;
	font-weight: 300;
`;

export const TableSummaryDiscount = styled.ul`
	height: 100%
`;

export const RowTableSummaryDiscount = styled.li`
	margin-top: 0px !important;
	padding-bottom: 10px;
`;