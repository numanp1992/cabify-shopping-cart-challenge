import styled from 'styled-components';

type ProductListTitle = {
	Row?: string,
}

export const ProductListTitle = styled.li<{Row: any}>`
	color: ${({ theme }) => theme.colours.H2productList};
	text-transform: uppercase;
	letter-spacing: 1px;
	font-size: 10px;
	line-height: 16px;

	${({ Row }) => Row && Row};
`;

export const ColProductTitle = styled.div<{width: string}>`
	width: ${({ width }) => width && width};
`;
