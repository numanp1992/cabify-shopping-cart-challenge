import styled from 'styled-components';

export const SummaryItemsNumber = styled.span`

`;

export const SummaryItemsPrice = styled.span`
	font-size: 16px;
	font-weight: bold;
`;