import styled from 'styled-components';

export const AppAtributes = styled.div<{Background: string}>`
	margin: 0;
	padding: 0;
	height: 100vh;
	background-image: ${({ Background }) => 'url(' + Background + ')'};
	background-color: ${({ theme }) => theme.colours.body};
	background-position: top left;
	background-size: cover;
	font-family: Avenir, "Avenir Next", "Segoe UI", sans-serif;
	font-weight: regular;
`;

export const AppContainer = styled.div`
	position: fixed;
	top: 50%;
	left: 50%;
	display: flex;
	overflow-x: hidden;
	overflow-y: auto;
	max-width: 1088px;
	max-height: 648px;
	width: calc(100% - 64px);
	height: calc(100% - 64px);
	border-radius: 4px;
	background-color: ${({ theme }) => theme.colours.white};
	transform: translate(-50%, -50%);
`;

export const Products = styled.section`
	flex: 1;
	padding: 40px 32px 40px 56px;
`;

export const Main = styled.h1`


	padding-bottom: 16px;
	border-bottom: 1px solid rgba(33, 34, 64, 0.16);
`;

export const ProductsList = styled.ul<{TableHead: any }>`
	position: relative;
	width: 100%;

	${({ TableHead }) => TableHead && TableHead};
`;

export const Summary = styled.aside`
	display: flex;
	flex-flow: column wrap;
	padding: 40px 32px;
	width: 312px;
	background-color: #f3f3f3;
	color: #212240;
	&& ul li {
		display: flex;
		justify-content: space-between;
		margin-top: -14px;
	}
	  
`;

export const SummaryItems = styled.ul<{Wrapper: any, Border: any }>`
	${({ Wrapper }) => Wrapper && Wrapper};
	${({ Border }) => Border && Border};
`;

export const SummaryDiscountsWrapper = styled.div<{Border: any, WrapperHalf: any}>`

	${({ WrapperHalf }) => WrapperHalf && WrapperHalf};
`;

export const SummaryTotalWrapper = styled.div<{Wrapper: any}>`

	${({ Wrapper }) => Wrapper && Wrapper};

	align-self: flex-end;
	margin-top: auto;
	padding-top: 16px;
	padding-bottom: 0;
	color: #212240;
	width: 100%;
	border-top: 1px solid rgba(33, 34, 64, 0.16);
	
`;

