import{ FC } from 'react'

//css styled components
import { 
	ColProduct, 
	ButtonContainer,
	CountButton, 
	ProductQuantityButton, 
} from '../styles/productListContainer.styles'

//redux
import { useDispatch } from 'react-redux';
import { 
    toAddProduct, 
    toRemoveProduct 
} from '../../store/actions/products'

interface Props {
	quantity?:  number;
	productCode: string | null; 
	productPrice: number | null; 
}



const ProductQuantity: FC<Props> =  ({ quantity, productCode, productPrice }) => {

	const dispatch = useDispatch();

	const removeItem = () => dispatch(toRemoveProduct({ productCode, quantity, productPrice }));

	const addItem = () => dispatch(toAddProduct({ productCode, quantity, productPrice }));
	
	return (
		<ColProduct width='20%'>
			<ButtonContainer>
			<CountButton
				onClick={ () =>{ removeItem() } }
			>
				-
			</CountButton>
			<label>
				<ProductQuantityButton
					readOnly
					value={ quantity } 
				>
				</ProductQuantityButton>
			</label>
			<CountButton
				onClick={ () =>{ addItem() } }
			>
				+
			</CountButton>
			</ButtonContainer>
		</ColProduct>
	)
  }
  
  export default ProductQuantity;