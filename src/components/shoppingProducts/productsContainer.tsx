import { FC } from 'react'

import ProductMap from './productMap'

interface Props {
	productStore: any;
}



const ProductsContainer: FC<Props> = ({ productStore }) => {

	let showProducts = [];

	for (const [i, product] of productStore.entries()) {
		if(product.productCode) showProducts.push(<ProductMap {...product} key={ i } />);
	}

	return (
		<>
			{ showProducts }
		</>
	)
  }
  
  export default ProductsContainer;