import { FC } from 'react'

//css styled components
import { 
	Product
} from '../styles/productListContainer.styles'

//css global styled components
import { Row } from '../styles/globalCss.styles'

import ProductDescription from './productDescription'
import ProductQuantity from './productQuantity'
import ProductPrice from './productPrice'
import ProductTotal from './productTotal'

interface Props {
	name: string;
	productCode: string;
	productPrice: number;
	quantity: number;
	currency: string;
	description: string;
	onlyProduct: Array<any> | null;
	totalPrice: number
}

const ProductMap: FC<Props> =  ({ name, productCode, productPrice, quantity, currency, description, totalPrice }) => {

	return (
		<Product Row={ Row }>
			<ProductDescription 
				name= { name }
				productCode= { productCode }
				productPrice= { productPrice }
				currency= { currency }
				description= { description }
				quantity={ quantity }
			/>
			<ProductQuantity 
				quantity={ quantity }
				productCode={ productCode }
				productPrice={ productPrice }
			/>
			<ProductPrice 
				productPrice= { productPrice }
				currency= { currency }
			/>
			<ProductTotal 
				totalPrice= { totalPrice }
				currency= { currency }
			/>
		</Product>
	)
  }
  
  export default ProductMap;