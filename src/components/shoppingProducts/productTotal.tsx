import { FC } from 'react'

//css styled components
import { 
	ColProduct, 
	ProductPriceContainer, 
	ProductCurrency 
} from '../styles/productListContainer.styles'

interface Props {
	currency: string | null;
	totalPrice?:  number;
}

const ProductTotal: FC<Props> =  ({ currency, totalPrice}) => {

	return (
		<ColProduct width='15%'> 
			<ProductPriceContainer>
				{ totalPrice }
			</ProductPriceContainer>
			<ProductCurrency>
				{ currency }
			</ProductCurrency>
		</ColProduct>
	)
  }
  
  export default ProductTotal;