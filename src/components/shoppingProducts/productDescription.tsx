import { FC, useContext } from 'react'

//modal
import { ModalContext } from "../modal/modalContext";
import ModalDescription from './modalDescription'

//css styled components
import { 
	ColProduct, 
	ProductImageContainer, 
	ProductImage, 
	ProductName, 
	ProductCode, 
} from '../styles/productListContainer.styles'

interface Props {
	name: string;
	productCode: string;
	productPrice: number;
	currency: string;
	description: string;
	quantity: number;
}

const ProductDescription: FC<Props> =  ({ name, productCode, productPrice, currency, description, quantity }) => {

	let { handleModal } = useContext(ModalContext);

	let image = require('../../img/' + name?.toLocaleLowerCase() + '.png')

	return (
		<ColProduct width='45%'
			onClick={ () => { handleModal(<ModalDescription image={ image } description={ description } productPrice={ productPrice } currency={ currency } productCode={ productCode } quantity={ quantity } name={ name }  />) } }
		>
			<ProductImageContainer>
				<ProductImage 
					src={ image }
				/>
				<div className="product-description">
					<ProductName>
						{ name }
					</ProductName>
					<ProductCode>
						Product Code { productCode }
					</ProductCode>
				</div>
			</ProductImageContainer>
		</ColProduct>
	)
  }
  
  export default ProductDescription;