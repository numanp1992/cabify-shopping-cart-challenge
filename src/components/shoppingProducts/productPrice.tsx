import { FC } from 'react'

//css styled components
import { 
	ColProduct, 
	ProductPriceContainer, 
	ProductCurrency 
} from '../styles/productListContainer.styles'

interface Props {
	productPrice: number | null;
	currency: string | null;
}

const ProductPrice: FC<Props> =  ({ productPrice, currency }) => {

	return (
		<ColProduct width='20%'> 
			<ProductPriceContainer>
				{ productPrice }
			</ProductPriceContainer>
			<ProductCurrency>
				{ currency }
			</ProductCurrency>
		</ColProduct>
	)
  }
  
  export default ProductPrice;