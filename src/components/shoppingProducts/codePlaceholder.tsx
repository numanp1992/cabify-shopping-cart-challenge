import { FC, useContext } from 'react';

//database
import { DISCOUNTS } from '../../database/database' 

//modal
import ModalReusable from '../summaryProducts/modalReusable'
import { ModalContext } from "../modal/modalContext";

//css styled components
import { 
	PlaceholderForm,
	InputPlaceholder,
	InputButton,
	InputLabel
} from '../styles/codePlaceholderContainer.styles'

//redux
import { useDispatch } from 'react-redux';
import { toAddDiscount } from '../../store/actions/products'

interface Props {

}

const CodePlaceholder: FC<Props> =  () => {

	const dispatch = useDispatch();

	let { handleModal } = useContext(ModalContext);

	const addDiscount = (submitDiscount:string) => {
		if(DISCOUNTS.hasOwnProperty(submitDiscount)) {dispatch(toAddDiscount({ submitDiscount }))}
		else { return handleModal(<ModalReusable success={ false } titleModal={'You enter an invalid code!'} />)}
	}

	return (
		<PlaceholderForm onSubmit={ (e:any) => { e.preventDefault(); addDiscount(e.target[0].value)}}>
			<InputLabel htmlFor="discount">Search</InputLabel>
			<InputPlaceholder id="discount" type="discount" placeholder="Enter discount code" />
			<InputButton className="button-discount">Submit</InputButton>
		</PlaceholderForm>
	)
  }
  
export default CodePlaceholder;