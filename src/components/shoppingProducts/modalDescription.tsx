import { FC } from 'react'

//css styled components
import { 
	ModalProductDescription, 
	ModalImageContainer, 
	ModalProductImage,
	ModalInfoContainer,
	ModalPriceContainer, 
	ModalDescriptionContainer,
	ModalProductCodeContainer
} from '../styles/productListContainer.styles'

import { SummaryTotalButton } from '../styles/summaryTotalContainer.styles'

//redux
import { useDispatch } from 'react-redux';
import { toAddProduct } from '../../store/actions/products'

interface Props {
	productPrice: number;
	currency: string;
	image: string;
	description: string;
	productCode: string;
	quantity: number;
	name: string;
}

const ModalDescription: FC<Props> =  ({ image, description, productPrice, currency, productCode, quantity, name }) => {

	const dispatch = useDispatch();

	const addItem = () => dispatch(toAddProduct({ productCode, quantity, productPrice }));

	return (
		<ModalProductDescription>
			<ModalImageContainer>
				<ModalProductImage 
					src={ image }
				/>
			</ModalImageContainer>
			<ModalInfoContainer>
				<ModalPriceContainer>
					{name} <div>{productPrice}{currency}</div>
				</ModalPriceContainer>
				<ModalDescriptionContainer>
					{ description }
				</ModalDescriptionContainer>
				<ModalProductCodeContainer>
					ProductCode {productCode}
				</ModalProductCodeContainer>
				<SummaryTotalButton
					comeFromModal= { true }
					onClick={ ()=>{ addItem() } }
				>
					Add Product
				</ SummaryTotalButton>
			</ModalInfoContainer>
		</ModalProductDescription>
	)
  }


  
export default ModalDescription;