import { FC, memo } from 'react'

import SummaryQuantityContainer from './summaryQuantityContainer'
import SummaryDiscountsContainer from './summaryDiscountsContainer'
import SummaryTotalContainer from './summaryTotalContainer'

//css styled components
import {
	SummaryItems,
	SummaryDiscountsWrapper,
	SummaryTotalWrapper
} from '../styles/appWrapper.styles'

import {
	H2SummaryDiscounts,
	TableSummaryDiscount,
} from '../styles/summaryDiscountsContainer.styles'

//css global styled components
import {
	Wrapper,
	Border,
} from '../styles/globalCss.styles'

interface Props {
	productStore: any;
}

const SummaryContainer: FC<Props> = memo(({ productStore }) => {

	let orderSummary = {
		totalItems: 0,
		totalPrice: 0,
		currency: '',
	}

	let discounts = 0

	let discountProducts = []

	let codeDiscount = '' 

	for (let product in productStore) {
		if(productStore[product].productCode){
			orderSummary.totalItems += productStore[product].quantity;
			orderSummary.totalPrice += productStore[product].totalPrice;
			orderSummary.currency = productStore[product].currency;
			productStore[product].discountShow ? discounts += productStore[product].discountTotal : discounts += 0;
		}

		if (productStore[product].hasDiscount && productStore[product].discountShow) discountProducts.push(<SummaryDiscountsContainer {...productStore[product]} key={productStore[product].productCode} />)

		if(productStore[product].hasOwnProperty('CABIFY10')){
			for(let discounts in productStore[product]){
				let discount = productStore[product][discounts]
				if(discount.discountApplied){
					codeDiscount = discount.discountLabel;
					discountProducts.push(<SummaryDiscountsContainer {...discount} key={discount.discountLabel} />)
				} 
			}
		}
	}

	return (
		<>
			<SummaryItems
				Wrapper={Wrapper}
				Border={Border}
			>
				<SummaryQuantityContainer
					{...orderSummary}
				/>
			</SummaryItems>
			<SummaryDiscountsWrapper
				Border={undefined}
				WrapperHalf={undefined}
			>
				<H2SummaryDiscounts>Discounts</H2SummaryDiscounts>
				<TableSummaryDiscount>
					{discountProducts}
				</TableSummaryDiscount>
			</SummaryDiscountsWrapper>
			<SummaryTotalWrapper
				Wrapper={Wrapper}
			>
				<SummaryTotalContainer
					totalPrice={orderSummary.totalPrice}
					discounts={discounts}
					codeDiscount={ codeDiscount }
				/>
			</SummaryTotalWrapper>
		</>
	)
})

export default SummaryContainer;