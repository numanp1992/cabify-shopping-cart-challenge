import { FC, useContext } from 'react'

//modal
import { ModalContext } from "../modal/modalContext";

//css styled components
import { 
	ModalCheckoutContainer, 
    SuccessImage,
    ButtonCloseCheckout,
} from '../styles/productListContainer.styles'

interface Props {
    success: boolean,
    titleModal: string,
}

const ModalReusable:FC<Props> =  ({ success, titleModal }) => {

    let { handleModal } = useContext(ModalContext);

	return (
		<ModalCheckoutContainer>
            {titleModal && titleModal}
            <SuccessImage 
                src={ success && success ? require('../../img/success.png') : require('../../img/warning.png') }
            />
            <ButtonCloseCheckout 
                onClick={ () => handleModal() }
            >
                Close
            </ButtonCloseCheckout>
		</ModalCheckoutContainer>
	)
  }
  
export default ModalReusable;