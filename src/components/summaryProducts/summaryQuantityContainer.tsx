import { FC } from 'react'

// css styled components
import { 
	SummaryItemsNumber,
	SummaryItemsPrice,
} from '../styles/summaryQuantityContainer.styles'


interface Props {
	totalItems: number;
	totalPrice: number;
	currency: string;
}

const SummaryQuantityContainer: FC<Props> = ({totalItems, totalPrice, currency}) => {

  return (
	<>
		<li>
			<SummaryItemsNumber>
				{totalItems} Items
			</SummaryItemsNumber>
			<SummaryItemsPrice>
				{ totalPrice } { currency }
			</SummaryItemsPrice>
		</li>
	</>
  )
}

export default SummaryQuantityContainer;
