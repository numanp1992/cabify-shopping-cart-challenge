import { FC } from 'react'

// css styled components
import { 
    RowTableSummaryDiscount
} from '../styles/summaryDiscountsContainer.styles'

interface Props {
    discountLabel: string[] | null;
    discountTotal: number,
    currency: string,
}

const SummaryDiscountsContainer: FC<Props> = ({ discountLabel, discountTotal, currency }) => {
  return (
    <>      
        <RowTableSummaryDiscount>
            <span>{ discountLabel }</span>
            <span>{ currency ? '-' : '' }{ discountTotal }{ currency }</span>
        </RowTableSummaryDiscount>
    </>
  )
}

export default SummaryDiscountsContainer;
