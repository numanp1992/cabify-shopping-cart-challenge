import { FC, useContext } from 'react'

//modal
import { ModalContext } from "../modal/modalContext";
import ModalReusable from "./modalReusable"

//css styled components
import { 
	SummaryTotalCost,
	SummaryTotalPrice,
	SummaryTotalButton,
} from '../styles/summaryTotalContainer.styles'


interface Props {
	totalPrice: number,
	discounts: number,
	codeDiscount: string
}

const SummaryTotalContainer:FC<Props> = ({ totalPrice, discounts, codeDiscount }) => {

	let { handleModal } = useContext(ModalContext);

	let priceApp = totalPrice - discounts

	if (codeDiscount) priceApp = codeDiscount === '10% discount' ? priceApp - (priceApp*10/100) : priceApp

	return (
	<>
		<ul>
			<li>
			<SummaryTotalCost>Total cost</SummaryTotalCost>
			<SummaryTotalPrice>{ priceApp }€</SummaryTotalPrice>
			</li>
		</ul>
		<SummaryTotalButton
			onClick={ () => handleModal(<ModalReusable success={ true } titleModal={'Congratulations! you bought the item/s!'} />) }
			disabled={ totalPrice === 0}
		>Checkout</SummaryTotalButton>
	</>
	)
}

export default SummaryTotalContainer;
