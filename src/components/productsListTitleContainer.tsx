//css global styled components
import { Row } from './styles/globalCss.styles'

//css styled components
import { ProductListTitle, ColProductTitle } from '../components/styles/productsListTitleContainer.styles'

const ProductsListTitleContainer = () => {
    return (
        <ProductListTitle
            Row={ Row }
        >
            <ColProductTitle width='45%'>
                Product details
            </ColProductTitle>
            <ColProductTitle width='20%'>
                Quantity
            </ColProductTitle>
            <ColProductTitle width='20%'>
                Price
            </ColProductTitle>
            <ColProductTitle width='15%'>
                Total
            </ColProductTitle>
        </ProductListTitle>
    )
  }
  
  export default ProductsListTitleContainer;