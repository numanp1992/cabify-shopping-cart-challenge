import React, { FC } from 'react'

import { toolTypes } from "./toolsTypes";

export const toAddProduct = ({  productCode, quantity, productPrice }:{productCode?:string | null, quantity?:number | null, productPrice?:number | null}) => {
	return {
	type: toolTypes.ADD_PRODUCT,
	productCode,
	quantity,
	productPrice,
	};
};

export const toRemoveProduct = ({ productCode, quantity, productPrice }:{productCode?:string | null, quantity?:number | null, productPrice?:number | null} ) => {
	return {
		type: toolTypes.REMOVE_PRODUCT,
		productCode,
		quantity,
		productPrice,
  };
};

export const toAddDiscount = ({ submitDiscount }:{submitDiscount:string} ) => {
	return {
		type: toolTypes.ADD_DISCOUNT,
		submitDiscount
  };
};