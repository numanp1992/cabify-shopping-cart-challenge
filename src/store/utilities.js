import { products } from '../database/database'
import { DISCOUNTS } from '../database/database'

export const getInitialState = () => {
    let state = []
    
    for(let product of products) {
        state.push(product)
    }
    state.push(DISCOUNTS)
    return state
}

export const newQuantity = (count, storeProduct, productCode) => {
    return count === 'add' ? { ...storeProduct, quantity: storeProduct.quantity + 1 } : checkDecrement(storeProduct) ? { ...storeProduct, quantity: storeProduct.quantity - 1 } : storeProduct
}

export const checkDecrement = (stateProduct) => {
    return stateProduct.quantity > 0;
}

export const newPrice = (newStore, count, state) => {
    return  { ...newStore, totalPrice: newStore.productPrice * newStore.quantity, discountShow: checkDiscount({...newStore }, count,  'discountShow', state), discountTotal: checkDiscount({...newStore }, count, 'discountTotal', state) }
}

export const checkDiscount = ({discountCondition, quantity, discountTotal, productPrice}, count, comeFrom, state) => {
    if( discountCondition === "THREE_OR_MORE" ){
        if(comeFrom === 'discountShow') return  quantity >= 3 ? true : false
        if(comeFrom === 'discountTotal') return calculateFivePercent({ productPrice, count, discountTotal })
    } else if( discountCondition === "TWO_FOR_ONE" ) {
        if(comeFrom === 'discountShow') return quantity >= 2 ? true : false
        if(comeFrom === 'discountTotal') return calculateTwoForOne({ productPrice, count, discountTotal, quantity })
    } else return 0
}

export const calculateFivePercent = ({productPrice, count, discountTotal}) => {
    return  count === 'add' ? discountTotal += (productPrice * 5/100) : discountTotal -= (productPrice * 5/100)
}

export const calculateTwoForOne = ({ quantity, productPrice }) => {
    return (Math.trunc(quantity / 2)) * productPrice;
}