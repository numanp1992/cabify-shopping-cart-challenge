import { toolTypes } from "../actions/toolsTypes";
import { getInitialState, newQuantity, newPrice } from '../utilities'
  
const reducerProducts = (state = getInitialState(), action) => {
	
	const handleProduct = (count) => {
		let stateProduct = state.find(stateKey => stateKey.productCode === action.productCode)
		if(!stateProduct) return state;
		return state.map((storeProduct, i) => {
			if (storeProduct.productCode === action.productCode) {
				return newPrice(newQuantity(count, storeProduct), count, storeProduct)
			}
			return storeProduct
		});
	}

	const handleDiscount = () => {
		return state.map((storeProduct, i) => {
			if(storeProduct.hasOwnProperty(action.submitDiscount)){
				return {
					...storeProduct,
					[action.submitDiscount]:{
						discountApplied: true,
						discountLabel: storeProduct[action.submitDiscount].discountLabel,
					}
				}
			} 
			return storeProduct
		});
	}

	switch(action.type) {
		case toolTypes.ADD_PRODUCT:
			return handleProduct('add');
		case toolTypes.REMOVE_PRODUCT:
			return handleProduct('subtract');
		case toolTypes.ADD_DISCOUNT:
			return handleDiscount();
		default:
			return state;
	};
};

export default reducerProducts