import { combineReducers } from "redux";
import products from "./products";

export const rootReducer = combineReducers({
  products: products
});

export type RootState = ReturnType<typeof rootReducer>