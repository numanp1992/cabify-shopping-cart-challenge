import React from 'react'
import { RootStateOrAny, useSelector } from 'react-redux';

//components
import ProductsListTitleContainer from './components/productsListTitleContainer'
import ProductsContainer from './components/shoppingProducts/productsContainer'
import SummaryContainer from './components/summaryProducts/summaryContainer'
import CodePlaceholder from './components/shoppingProducts/codePlaceholder'

import { ModalProvider } from "./components/modal/modalContext";

//assets
import Background from './img/background.png'

//css global styled components
import { 
	TableHead,
} from './components/styles/globalCss.styles'

//css styled components
import { 
	AppAtributes, 
	AppContainer, 
	Products, 
	Main, 
	ProductsList,
	Summary,
} from './components/styles/appWrapper.styles'

const App = () => {

	const productStore = useSelector( (state: RootStateOrAny) => state.products);

	return (
	<AppAtributes 
		Background={Background}
	>
		<AppContainer>
			<ModalProvider>
			<Products>
				<Main>
					Shopping cart
				</Main>
				<ProductsList
					TableHead={ TableHead }
				>
					<ProductsListTitleContainer />
				</ProductsList>
				<ProductsList
					TableHead={ null }
				>
					<ProductsContainer
						productStore={ productStore }
					/>
				</ProductsList>
				<CodePlaceholder />
			</Products>
			<Summary>
				<Main>
					Order Summary
				</Main>
				<SummaryContainer 
					productStore={ productStore }
				/>
			</Summary>
			</ModalProvider>
		</AppContainer>
	</AppAtributes>
	)
}

export default App;
